import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:sm_basic/bloc_1/bloc_actions.dart';
import 'package:sm_basic/bloc_1/person.dart';
import 'package:sm_basic/bloc_1/persons_bloc.dart';

const mockedPersons1 = [
  Person(name: 'zbychu1', age: 11),
  Person(name: 'dave1', age: 43),
];

const mockedPersons2 = [
  Person(name: 'zbychu2', age: 11),
  Person(name: 'dave2', age: 43),
];

Future<Iterable<Person>> mockGetPersons1(String _) =>
    Future.value(mockedPersons1);

Future<Iterable<Person>> mockGetPersons2(String _) =>
    Future.value(mockedPersons2);

void main() {
  group('Testing bloc', () {
    late PersonsBloc bloc;
    setUp(() {
      bloc = PersonsBloc();
    });
    blocTest<PersonsBloc, FethResult?>(
      'Test initial state',
      build: () => bloc,
      verify: (bloc) => expect(bloc.state, null),
      //expect: () => bloc.state != null,
    );
    blocTest<PersonsBloc, FethResult?>(
      'Mock retrving persons from first iterable',
      build: () => bloc,
      act: (bloc) {
        bloc.add(const LoadPersonsAction(
          loader: mockGetPersons1,
          url: 'dummy',
        ));
        bloc.add(const LoadPersonsAction(
          loader: mockGetPersons1,
          url: 'dummy',
        ));
      },
      expect: () => const [
        FethResult(
          isRetrivedFromCache: false,
          persons: mockedPersons1,
        ),
        FethResult(
          isRetrivedFromCache: true,
          persons: mockedPersons1,
        )
      ],
      // verify: (bloc) => bloc.state == null,
    );

    blocTest<PersonsBloc, FethResult?>(
      'Mock retrving persons from second iterable',
      build: () => bloc,
      act: (bloc) {
        bloc.add(const LoadPersonsAction(
          loader: mockGetPersons2,
          url: 'dummy',
        ));
        bloc.add(const LoadPersonsAction(
          loader: mockGetPersons2,
          url: 'dummy',
        ));
      },
      expect: () => const [
        FethResult(
          isRetrivedFromCache: false,
          persons: mockedPersons2,
        ),
        FethResult(
          isRetrivedFromCache: true,
          persons: mockedPersons2,
        )
      ],
      // verify: (bloc) => bloc.state == null,
    );
  });
}
