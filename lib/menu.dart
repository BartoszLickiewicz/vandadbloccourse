import 'package:flutter/material.dart';

class Menu extends StatelessWidget {
  final String title;
  const Menu({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[200],
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.blue,
      ),
      body: Container(
        child: Column(children: [
          Card(
            child: ListTile(
              title: Text('Conacts'),
              onTap: () => Navigator.of(context).pushNamed('/contacts'),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('Home Page'),
              onTap: () => Navigator.of(context).pushNamed('/home-page'),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('Cubit'),
              onTap: () => Navigator.of(context).pushNamed('/cubit'),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('Bloc 1'),
              onTap: () => Navigator.of(context).pushNamed('/bloc-1'),
            ),
          ),
          Card(
            child: ListTile(
              title: Text('Bloc 2'),
              onTap: () => Navigator.of(context).pushNamed('/bloc-2'),
            ),
          ),
        ]),
      ),
    );
  }
}
