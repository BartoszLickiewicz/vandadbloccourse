import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sm_basic/bloc_1/persons_bloc.dart';
import 'package:sm_basic/menu.dart';
import 'package:sm_basic/sm/cubit_test.dart';
import 'package:sm_basic/sm/inh_wid.dart';
import 'package:sm_basic/sm/sm_basic.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const Menu(title: 'Flutter Demo Home Page'),
      routes: {
        '/contacts': (context) => const ContactView(
              title: '',
            ),
        '/contacts/new-contact': (context) => const NewContactView(),
        '/home-page': (context) => MockApiProvider(
              child: const HomePage(),
              api: MockApi(),
            ),
        '/cubit': (context) => CubitTest(),
        '/bloc-1': (context) => BlocProvider(
              create: (_) => PersonsBloc(),
              child: BlocTestApi(),
            ),
        '/bloc-2': (context) => BlocProvider(
              create: (_) => NotesBloc(),
              child: BlocNotes(),
            ),
      },
    );
  }
}
