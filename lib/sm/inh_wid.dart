import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ValueKey _textKey = const ValueKey<String?>(null);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(MockApiProvider.of(context)?.api.dateTime ?? ''),
      ),
      body: GestureDetector(
        onTap: () async {
          final mockapiprovider = MockApiProvider.of(context);
          final api = MockApiProvider.of(context)?.api;
          final dateTime = await api?.getDataTime();
          print(mockapiprovider);
          setState(() {
            _textKey = ValueKey(dateTime);
          });
        },
        child: SizedBox.expand(
          child: Container(
            color: Colors.amber,
            child: DateTimeWidget(key: _textKey),
          ),
        ),
      ),
    );
  }
}

class MockApi {
  String? dateTime;
  Future<String> getDataTime() {
    return Future.delayed(
      const Duration(seconds: 1),
      () => DateTime.now().toIso8601String(),
    ).then((value) {
      dateTime = value;
      return value;
    });
  }

  MockApi();
}

class MockApiProvider extends InheritedWidget {
  final MockApi api;
  final String uuid;

  static MockApiProvider? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MockApiProvider>();
  }

  MockApiProvider({super.key, required super.child, required this.api})
      : uuid = const Uuid().v4();
  @override
  bool updateShouldNotify(covariant MockApiProvider oldWidget) {
    return uuid != oldWidget.uuid;
  }
}

class DateTimeWidget extends StatelessWidget {
  const DateTimeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final api = MockApiProvider.of(context)?.api;
    return Text(api?.dateTime ?? 'Tap on screen');
  }
}
