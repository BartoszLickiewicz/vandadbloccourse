import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class Contact {
  final String id;
  final String name;
  Contact({required this.name}) : id = const Uuid().v4();
}

class ContactBook extends ValueNotifier<List<Contact>> {
  ContactBook._sharedInstance() : super([]);
  static final ContactBook _shared = ContactBook._sharedInstance();
  factory ContactBook() => _shared;

  int get length => value.length;

  void add({required contact}) {
    value.add(contact);
    notifyListeners();
  }

  void remove({required contact}) {
    value.remove(contact);
    notifyListeners();
  }

  Contact? contact({required int position}) =>
      value.length > position ? value[position] : null;
}

class ContactView extends StatelessWidget {
  const ContactView({super.key, required this.title});

  final String title;

  Widget build(BuildContext context) {
    final contactBook = ContactBook();
    return Scaffold(
      appBar: AppBar(
        title: const Text("sm basic"),
        backgroundColor: Colors.purple[200],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await Navigator.of(context).pushNamed('/contacts/new-contact');
        },
        child: const Icon(Icons.add),
      ),
      body: ValueListenableBuilder(
          valueListenable: ContactBook(),
          builder: (context, value, child) {
            final contacts = value;
            return ListView.builder(
                itemCount: contactBook.length,
                itemBuilder: (context, index) {
                  final contact = contacts[index];
                  return Dismissible(
                      onDismissed: (direction) {
                        ContactBook().remove(contact: contact);
                      },
                      key: ValueKey(contact.id),
                      child: Card(
                          child: ListTile(title: Text(contact?.name ?? ""))));
                });
          }),
    );
  }
}

class NewContactView extends StatefulWidget {
  const NewContactView({super.key});

  @override
  State<NewContactView> createState() => _NewContactViewState();
}

class _NewContactViewState extends State<NewContactView> {
  late final TextEditingController _controller;
  @override
  void initState() {
    _controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add new Contact"),
      ),
      body: Column(children: [
        TextField(
          controller: _controller,
          decoration: const InputDecoration(
              hintText: "Enter a new contact name here..."),
        ),
        TextButton(
            onPressed: () {
              final contact = Contact(name: _controller.text);
              ContactBook().add(contact: contact);
              Navigator.of(context).pop();
            },
            child: Text("Add contact"))
      ]),
    );
  }
}
