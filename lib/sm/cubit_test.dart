import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';

class CubitTest extends StatefulWidget {
  CubitTest({super.key});

  @override
  State<CubitTest> createState() => _CubitTestState();
}

class _CubitTestState extends State<CubitTest> {
  late final MySubjectCubit cubit;
  @override
  void initState() {
    super.initState();
    cubit = MySubjectCubit();
  }

  @override
  void dispose() {
    cubit.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[100],
      appBar: AppBar(
        backgroundColor: Colors.blue[300],
        title: const Text("Cubit Test"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            StreamBuilder<String?>(
                stream: cubit.stream,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                      return Text('brak');
                    case ConnectionState.waiting:
                      return Text('kliknij poniżej');
                    case ConnectionState.active:
                      return Text(snapshot.data!);
                    case ConnectionState.done:
                      return SizedBox();
                  }
                }),
            const SizedBox(
              height: 10,
            ),
            FilledButton(
                onPressed: () => cubit.pickRandomName(), child: Text("Click")),
          ],
        ),
      ),
    );
  }
}

List<String> names = ['bartosz', 'zbyszek', 'kondziu'];

extension randomElement<T> on Iterable<T> {
  T getRandom() => elementAt(Random().nextInt(length));
}

class MySubjectCubit extends Cubit<String?> {
  MySubjectCubit() : super(null);

  void pickRandomName() {
    String? newName;

    do {
      newName = names.getRandom();
    } while (newName == state);
    emit(newName);
  }
}
