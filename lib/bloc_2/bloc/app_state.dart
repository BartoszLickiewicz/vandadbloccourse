import 'package:flutter/foundation.dart';
import 'package:sm_basic/bloc_2/models.dart';

@immutable
class AppState {
  final bool isLoading;
  final LoginErrors? loginError;
  final LoginHandle? loginHandle;
  final Iterable<Note>? fethNotes;

  const AppState(
      {required this.isLoading,
      required this.loginError,
      required this.loginHandle,
      required this.fethNotes});

  const AppState.empty()
      : isLoading = false,
        loginError = null,
        loginHandle = null,
        fethNotes = null;

  @override
  String toString() => {
        'isLoading': isLoading,
        'loginError': loginError,
        'loginHandle': loginHandle,
        'fethNotes': fethNotes,
      }.toString();
}
