import 'package:flutter/foundation.dart';
import 'package:sm_basic/bloc_2/models.dart';

@immutable
abstract class NotesApiProtocol {
  const NotesApiProtocol();
  Future<Iterable<Note>?> getNotes({required LoginHandle loginHandle});
}

@immutable
class NotesApi implements NotesApiProtocol {
  const NotesApi._sharedInstance();
  static const NotesApi _shared = NotesApi._sharedInstance();
  factory NotesApi.instance() => _shared;

  @override
  Future<Iterable<Note>?> getNotes({required LoginHandle loginHandle}) =>
      Future.delayed(const Duration(seconds: 2),
          () => loginHandle == const LoginHandle.fooBar() ? mockNotes : null);
}
