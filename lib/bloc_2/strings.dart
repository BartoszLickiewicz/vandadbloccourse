const emailOrPasswordEmptyDialogTitle =
    'Fill your password and the email field';
const emailOrPasswordEmptyDescription =
    'You have not entered your credentials. Fill your password and the email field';
const loginErrorDialogTitle = 'Login error';
const loginErrorDialogContent =
    'Invalid credentials. Fill your password and the email field';
const pleaseWait = 'Please wait...';
const enterYourPasswordHere = 'Enter your password here...';
const enterYourEmailHere = 'Enter your email here...';
const ok = 'OK';
const login = 'Log in';
const homePage = 'Home Page';
