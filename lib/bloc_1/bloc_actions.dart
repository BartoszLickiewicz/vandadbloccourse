import 'package:flutter/foundation.dart';
import 'package:sm_basic/bloc_1/person.dart';
import 'package:sm_basic/bloc_1/persons_bloc.dart';

const persons1Url = 'http://127.0.0.1:5500/lib/bloc_1/api/person1.json';
const persons2Url = 'http://127.0.0.1:5500/lib/bloc_1/api/person2.json';

typedef PersonsLoader = Future<Iterable<Person>> Function(String url);

// enum PersonUrl {
//   person1,
//   person2,
// }

// extension UrlString on PersonUrl {
//   String get urlString {
//     switch (this) {
//       case PersonUrl.person1:
//         return 'http://127.0.0.1:5500/lib/bloc_1/api/person1.json';
//       case PersonUrl.person2:
//         return 'http://127.0.0.1:5500/lib/bloc_1/api/person2.json';
//     }
//   }
// }

@immutable
abstract class LoadAction {
  const LoadAction();
}

@immutable
class LoadPersonsAction implements LoadAction {
  final String url;
  final PersonsLoader loader;
  const LoadPersonsAction({
    required this.loader,
    required this.url,
  });
}

@immutable
class FethResult {
  final Iterable<Person> persons;
  final bool isRetrivedFromCache;
  const FethResult({required this.isRetrivedFromCache, required this.persons});

  @override
  String toString() =>
      'FetchResult (isRetrivedFromCache = $isRetrivedFromCache, persons = $persons)';

  @override
  bool operator ==(covariant FethResult other) =>
      persons.isEqualToIgnoringOrdering(other.persons) &&
      isRetrivedFromCache == other.isRetrivedFromCache;

  @override
  int get hashCode => Object.hash(persons, isRetrivedFromCache);
}
