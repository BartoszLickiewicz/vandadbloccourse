import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sm_basic/bloc_1/bloc_actions.dart';
import 'dart:developer' as devtools show log;

import 'package:sm_basic/bloc_1/person.dart';

extension Log on Object {
  void log() => devtools.log(toString());
}

class PersonsBloc extends Bloc<LoadAction, FethResult?> {
  final Map<String, Iterable<Person>> _cache = {};
  PersonsBloc() : super(null) {
    on<LoadPersonsAction>((event, emit) async {
      print('action: LoadPersonsAction');
      final url = event.url;
      if (_cache.containsKey(url)) {
        final cachedPersons = _cache[url]!;
        final result =
            FethResult(isRetrivedFromCache: true, persons: cachedPersons);
        emit(result);
      } else {
        final loader = event.loader;
        final persons = await loader(url);
        _cache[url] = persons;
        final result = FethResult(isRetrivedFromCache: false, persons: persons);
        emit(result);
      }
    });
  }
}

extension Subscript<T> on Iterable<T> {
  T? operator [](int index) => length > index ? elementAt(index) : null;
}

extension IsEqualToIgnorinfOrdering<T> on Iterable<T> {
  bool isEqualToIgnoringOrdering(Iterable<T> other) =>
      length == other.length &&
      {...this}.intersection({...other}).length == length;
}

Future<Iterable<Person>> getPersons(String url) => HttpClient()
    .getUrl(Uri.parse(url))
    .then((req) => req.close())
    .then((resp) => resp.transform(utf8.decoder).join())
    .then((str) => json.decode(str) as List<dynamic>)
    .then((list) => list.map((e) => Person.fromjson(e)));

class BlocTestApi extends StatelessWidget {
  const BlocTestApi({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            BlocBuilder<PersonsBloc, FethResult?>(
              buildWhen: (previous, current) {
                final isRebuilded = previous?.persons != current?.persons;
                print('rebuild: $isRebuilded');
                return isRebuilded;
              },
              builder: (context, state) {
                state?.log();
                final persons = state?.persons;
                if (persons == null) {
                  return Text("no persons");
                } else {
                  return Expanded(
                    child: ListView.builder(
                        itemCount: persons.length,
                        itemBuilder: (context, index) {
                          return Card(
                              child: ListTile(
                            title: Text(persons[index]!.name),
                            subtitle: Text(persons[index]!.age.toString()),
                          ));
                        }),
                  );
                }
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                    onPressed: () => context.read<PersonsBloc>().add(
                        const LoadPersonsAction(
                            url: persons1Url, loader: getPersons)),
                    child: const Text('load json #1')),
                TextButton(
                    onPressed: () => context.read<PersonsBloc>().add(
                        const LoadPersonsAction(
                            url: persons2Url, loader: getPersons)),
                    child: const Text('load json #2')),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
